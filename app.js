const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


mongoose.connect("mongodb://" + process.env.MONGO_USR + ":" + process.env.MONGO_PSW + "@" + process.env.MONGO_URL, {
    useNewUrlParser: true
}).then((res) => {
    console.log("Connected to Database Successfully.");
}).catch((e) => {
    console.error("Conntection to database failed.");
    console.error(e.message);
});

app.use(morgan('dev'));

app.use('/hello-world', (req, res, next) => {
  res.status(200).json({
    message: "Hello World!"
  });
});

module.exports = app;
